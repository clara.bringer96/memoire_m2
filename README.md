# Memoire_M2

Master 2 Technologie des Langues - Université de Strasbourg.  
Mémoire sur la lemmatisation et l'annotation des formes verbales du français médiéval.  
Projet encadré par Mesdames Delphine Bernhard et Julie Glikman.