import re
import xml.etree.ElementTree as ET
from typing import List, Optional, Any, Dict, Tuple, Set

import bs4
import requests
from bs4 import BeautifulSoup


class DerivedVerbs:
    @staticmethod
    def get_prefixed_form(form: str, prefix: Tuple[str, str]) -> str:
        """
        Replace the prefix of the form
        :param form: form to replace
        :param prefix: tuple of the prefix to remove, and the prefix to add
        :return: string
        """
        p = re.match(rf"{prefix[0]}(.*)", form)
        to_write = prefix[1] + p.group(1) if p else form

        return to_write

    @staticmethod
    def get_common_stemm(verb1: str, verb2: str) -> str:
        """
        Get the common stemm between two verbs
        :param verb1: first verb
        :param verb2: second verb
        :return: string
        """
        if verb1 == verb2:
            return verb1

        v1 = len(verb1) - 1
        v2 = len(verb2) - 1
        while v1 >= 0 and v2 >= 0:
            if verb1[v1] != verb2[v2]:
                return verb1[v1 + 1:]
            v1 -= 1
            v2 -= 1

        return verb1 if len(verb1) < len(verb2) else verb2

    @staticmethod
    def get_max_common_stemm(verb: str, verbs: List[str]) -> Tuple[str, str]:
        """
        Get the longest common stemm between a verb and a list of verbs, and the verb from the list giving this stemm
        :param verb: verb
        :param verbs: list of verbs
        :return: tuple of strings
        """
        stemm = ""
        inf = ""
        for v in verbs:
            s = DerivedVerbs.get_common_stemm(verb, v)
            if len(s) > len(stemm):
                stemm = s
                inf = v

        return stemm, inf

    @staticmethod
    def get_prefixes_to_change(derived: List[str], infinitives: List[str]) -> Optional[Set[Tuple[str, str]]]:
        """
        Get all the prefixes that need to be changed
        :param derived: list of the derived verbs
        :param infinitives: list of all the infinitive forms of the verb
        :return: set of tuples, containing the prefix to cut and the prefix to add
        """
        prefixes_to_change = set()
        for der in derived:
            max_stemm, max_inf = DerivedVerbs.get_max_common_stemm(der, infinitives)
            if der in infinitives:
                prefixes_to_change.add(("", ""))
            else:
                d = re.match(rf"^(.*){max_stemm}$", der)
                if d:
                    der_pref = d.group(1)
                else:
                    return None

                i = re.match(rf"^(.*){max_stemm}$", max_inf)
                if i:
                    inf_pref = i.group(1)
                else:
                    return None

                prefixes_to_change.add((inf_pref, der_pref))

        return prefixes_to_change

    @staticmethod
    def get_derived_verbs(soup: BeautifulSoup, verb: str) -> List[str]:
        """
        Get all the derived verb of a page
        :param soup: BeautifulSoup object of the current page
        :param verb: verb of the current page
        :return: list of strings
        """
        derived_verbs = ["Verbes dérivés", "Derived verbs"]

        verb = verb.strip()
        span_content = soup.find("span", attrs={"class": "identified"})
        if span_content:
            flag_derived = 1 if span_content.text.strip() in derived_verbs else 0
            while flag_derived == 0:
                span_content = span_content.find_next("span", attrs={"class": "identified"})
                if span_content:
                    flag_derived = 1 if span_content.text.strip() in derived_verbs else 0
                else:
                    flag_derived = 2

            if flag_derived == 1:
                content = span_content.find_next("optgroup", attrs={"label": re.compile(rf"^{verb.upper()}.*")})

                derived_verbs = [v.strip() for v in content.text.split("\n\n") if v != ""] if content else []
                derived_verbs.append(verb)
                return derived_verbs

            return [verb]

        return [verb]


class MorphalouLikeBDD:
    tenses = {
        "Infinitif": {"mood": "infinitive"},
        "Participe Présent": {"mood": "participle", "tense": "present"},
        "Participe Passé": {"mood": "participle", "tense": "past"},
        "Indicatif Présent": {"mood": "indicative", "tense": "present"},
        "Indicatif Passé simple": {"mood": "indicative", "tense": "simplePast"},
        "Indicatif Imparfait": {"mood": "indicative", "tense": "imperfect"},
        "Indicatif Passé archaïque": {"mood": "indicative", "tense": "archaicPast"},
        "Indicatif Futur": {"mood": "indicative", "tense": "future"},
        "Subjonctif Présent": {"mood": "subjunctive", "tense": "present"},
        "Subjonctif Imparfait": {"mood": "subjunctive", "tense": "imperfect"},
        "Conditionnel": {"mood": "conditional"},
        "Impératif": {"mood": "imperative"}
    }

    persons = {
        "je": {"person": "firstPerson", "number": "singular"},
        "tu": {"person": "secondPerson", "number": "singular"},
        "il": {"person": "thirdPerson", "number": "singular"},
        "nous": {"person": "firstPerson", "number": "plural"},
        "vous": {"person": "secondPerson", "number": "plural"},
        "ils": {"person": "thirdPerson", "number": "plural"}
    }

    url_tcaf = "http://micmap.org/dicfro/search/tableaux-de-conjugaison/"
    lexicon = ET.Element("lexicon")

    orig_prettify = bs4.BeautifulSoup.prettify
    regex_prettify = re.compile(r'^(\s*)', re.MULTILINE)

    def prettify(self, encoding: Optional[str] = None, formatter: str = "minimal", indent_width: int = 4) -> str:
        return MorphalouLikeBDD.regex_prettify.sub(r'\1' * indent_width,
                                                   MorphalouLikeBDD.orig_prettify(self, encoding, formatter))

    bs4.BeautifulSoup.prettify = prettify

    @staticmethod
    def split_non_empty(to_split: str, regex: str) -> List[Any]:
        """
        Split a list given a regular expression, with no empty elements
        :param to_split: string to split
        :param regex: regular expression
        :return: list
        """
        return [e for e in re.split(regex, to_split) if e != ""]

    @staticmethod
    def pass_br(tag):
        while tag.name == "br":
            tag = tag.find_next()
        return tag

    @staticmethod
    def build_forms(all_conjugation_forms: Dict[str, List[Dict[str, Dict[str, str]]]], forms: str,
                    tense: Dict[str, str], person: Optional[Dict[str, str]] = None) \
            -> Dict[str, List[Dict[str, Dict[str, str]]]]:
        """
        Build a dictionary containing the tense and person for all verbal form
        :param all_conjugation_forms: dictionary to fill
        :param forms: a line of forms
        :param tense: tense of the line
        :param person: person of the line
        :return: tense and person for all verbal form
        """
        all_forms = MorphalouLikeBDD.split_non_empty(forms, r"[ ,;\)]")
        if person:
            conjugation = {"tense": tense, "person": person}
        else:
            conjugation = {"tense": tense}
        for form in all_forms:
            if form not in all_conjugation_forms:
                all_conjugation_forms[form] = list()
                all_conjugation_forms[form].append(conjugation)
            elif form in all_conjugation_forms and conjugation not in all_conjugation_forms[form]:
                all_conjugation_forms[form].append(conjugation)

        return all_conjugation_forms

    def get_forms(self, soup: BeautifulSoup) -> Optional[Dict[str, List[Dict[str, Dict[str, str]]]]]:
        """
        Get all the tense and person for all verbal form
        :param soup: BeautifulSoup object
        :return: dictionary containing the tense and person for all verbal form
        """
        content = soup.find("div", id="content")
        if content is not None:
            all_conjugation_forms: Dict[str, List[Dict[str, Dict[str, str]]]] = dict()

            verbs = content.find_all("div", attrs={"class": "verb"})

            for verb in verbs:
                tense_line = MorphalouLikeBDD.pass_br(verb.find_next())
                conjugation_line = MorphalouLikeBDD.pass_br(tense_line.find_next())

                while tense_line is not None and conjugation_line is not None and tense_line.name == "div" \
                        and "class" in tense_line.attrs and "tense" in tense_line["class"] \
                        and conjugation_line.name == "div" and "class" in conjugation_line.attrs \
                        and "conjugation" in conjugation_line["class"]:
                    text_tense = tense_line.text
                    if text_tense in self.tenses:
                        tense = self.tenses[text_tense]
                    else:
                        print("Temps inconnu !", text_tense)
                        return None

                    text_conjugation = conjugation_line.text
                    if text_conjugation[0] == "(":
                        all_persons = MorphalouLikeBDD.split_non_empty(text_conjugation, r"\(")
                        previous_p = None
                        for pers in all_persons:
                            split_pers = tuple(MorphalouLikeBDD.split_non_empty(pers, r"\)"))
                            if len(split_pers) == 1:
                                p = split_pers[0]
                                forms = ""
                            else:
                                p, forms = split_pers

                            if p.strip() in self.persons:
                                previous_p = p
                                person = self.persons[p]
                            else:
                                forms += f" {p}"
                                if previous_p is not None:
                                    p = previous_p
                                    person = self.persons[p]
                                else:
                                    person = None

                            all_conjugation_forms = MorphalouLikeBDD.build_forms(all_conjugation_forms, forms, tense,
                                                                                 person)
                    else:
                        all_conjugation_forms = MorphalouLikeBDD.build_forms(all_conjugation_forms, text_conjugation,
                                                                             tense)

                    tense_line = MorphalouLikeBDD.pass_br(conjugation_line.find_next())
                    conjugation_line = MorphalouLikeBDD.pass_br(tense_line.find_next())

            return all_conjugation_forms

        return None

    def search_verb(self, verb: str) \
            -> Optional[Tuple[Optional[Dict[str, List[Dict[str, Dict[str, str]]]]], List[str]]]:
        """
        Get the forms and the derived forms of a verb
        :param verb: verb to search
        :return: tuple of list of forms, and list of derived forms
        """
        url_page = self.url_tcaf + verb

        try:
            page = requests.get(url_page)
        except requests.exceptions.RequestException:
            print("L'URL fournie n'est pas valide.", url_page)
            return None
        else:
            soup = BeautifulSoup(page.text, "html.parser")

            derived_verbs = DerivedVerbs.get_derived_verbs(soup, verb)
            forms = self.get_forms(soup)

            return forms, derived_verbs

    @staticmethod
    def get_infinitives(verbal_forms: Dict[str, List[Dict[str, Dict[str, str]]]]) -> List[str]:
        """
        Get all infinitives for a verb
        :param verbal_forms: dictionary of all verbs
        :return: list of infinitives
        """
        infinitives = []
        for form, inflections in verbal_forms.items():
            for inflection in inflections:
                if "tense" in inflection and "mood" in inflection["tense"] and "tense" not in inflection["tense"] \
                        and "person" not in inflection:
                    infinitives.append(form)

        return infinitives

    def write_forms(self, infinitive: str, verbal_forms: Dict[str, List[Dict[str, Dict[str, str]]]],
                    prefixes_to_change: Set[Tuple[str, str]]):
        """
        Write all forms of a verb into XML
        :param infinitive: infinitive to write
        :param verbal_forms: dictionary of all the possible infinitive forms
        :param prefixes_to_change: set of the prefixes to change
        :return: None
        """
        for prefix in prefixes_to_change:
            prefixed_infinitive = DerivedVerbs.get_prefixed_form(infinitive, prefix)
            lexicon_entry = ET.SubElement(self.lexicon, "lexicalEntry", id=prefixed_infinitive)
            formset = ET.SubElement(lexicon_entry, "formSet")

            lemmatized_form = ET.SubElement(formset, "lemmatizedForm")
            lemmatized_orthography = ET.SubElement(lemmatized_form, "orthography")
            lemmatized_orthography.text = prefixed_infinitive
            grammatical_category = ET.SubElement(lemmatized_form, "grammaticalCategory")
            grammatical_category.text = "verb"

            for form, inflections in verbal_forms.items():
                for inflection in inflections:
                    to_write = DerivedVerbs.get_prefixed_form(form, prefix)
                    tense = inflection["tense"]
                    inflected_form = ET.SubElement(formset, "inflectedForm")

                    orthography = ET.SubElement(inflected_form, "orthography")
                    orthography.text = to_write

                    grammatical_mood = ET.SubElement(inflected_form, "grammaticalMood")
                    grammatical_mood.text = tense["mood"]

                    if "tense" in tense:
                        grammatical_tense = ET.SubElement(inflected_form, "grammaticalTense")
                        grammatical_tense.text = tense["tense"]

                    if "person" in inflection:
                        person = inflection["person"]

                        grammatical_number = ET.SubElement(inflected_form, "grammaticalNumber")
                        grammatical_number.text = person["number"]

                        grammatical_person = ET.SubElement(inflected_form, "grammaticalPerson")
                        grammatical_person.text = person["person"]

    def write_infinitive(self, infinitive: str):
        """
        Write a verb into XML
        :param infinitive: verb to write
        :return: None
        """
        verbal_forms, derived_verbs = self.search_verb(infinitive.strip())

        if verbal_forms and derived_verbs:
            inf_forms = MorphalouLikeBDD.get_infinitives(verbal_forms)

            prefixes_to_change = DerivedVerbs.get_prefixes_to_change(derived_verbs, inf_forms)
            if prefixes_to_change:
                self.write_forms(infinitive, verbal_forms, prefixes_to_change)

    def write_all_infinitives(self):
        """
        Write all verbs into XML
        :return: None
        """
        verbs_file = open("tcaf_table_contents_formated.txt", "r", encoding="utf8")
        for infinitive in verbs_file.readlines():
            self.write_infinitive(infinitive)

    def write_bdd(self, bdd_file):
        """
        Write XML element into a text file
        :param bdd_file: file to write the XML into
        :return: None
        """
        self.write_all_infinitives()
        bdd = ET.tostring(self.lexicon)

        with open(bdd_file, "w", encoding="utf8") as output:
            output.write(BeautifulSoup(bdd, "xml").prettify(formatter="xml"))


if __name__ == '__main__':
    morphalou_bdd = MorphalouLikeBDD()

    bdd_file = "morphalou_like_bdd.xml"
    morphalou_bdd.write_bdd(bdd_file)
