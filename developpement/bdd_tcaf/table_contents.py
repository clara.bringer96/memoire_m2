def read_table_contents():
    """
    Read the table of content copied from the PDF document, format it and rewrite it in a new text document
    :return: None
    """
    # File containing the table of content copied from the PDF document
    with open("tcaf_table_contents_to_format.txt", "r", encoding="utf8") as f:
        # File to write the formatted verion of the table of content
        with open("tcaf_table_contents_formated.txt", "w", encoding="utf8") as output:
            lines = f.readlines()
            for i in range(len(lines)):
                line = lines[i].split(" ")
                new_line = ""
                for j in range(len(line)):
                    is_number = False
                    is_parenthesis = False
                    for char in line[j]:
                        if '0' <= char <= '9':
                            is_number = True
                        if char == '(' or char == ')':
                            is_parenthesis = True
                    is_dot = line[j] == '.'

                    if not is_dot and not is_number and not is_parenthesis:
                        for char in line[j]:
                            if char != '\n' and char != '.':
                                new_line += char

                output.write(new_line.lower() + ("\n" if new_line != "" else ""))


if __name__ == '__main__':
    read_table_contents()
