import csv
import re

is_verb = re.compile(r"^VER")
uppercase = re.compile(r".*[A-Z].*")
get_inf = re.compile(r"(.+)\d+$")

tagged_lgerm = "tagged_utf8_qgraal_LGeRM.csv"

cattex09_verb_cjg = "VERcjg"
cattex09_verb_inf = "VERinf"
cattex09_verb_ppe = "VERppe"
cattex09_verb_ppa = "VERppa"

def is_annotated_verb(row):
    if len(row) <= 4:
        return False
    for r in row[4:]:
        if len(r) > 0:
            return True
    return False


def get_tagged_stats():
    nb_verbs, nb_tagged, nb_tagged_inf, nb_tagged_ppa, nb_tagged_ppe, nb_tagged_cjg = 0, 0, 0, 0, 0, 0

    with open(tagged_lgerm, newline="") as file:
        reader = csv.reader(file, delimiter=";", quotechar=None)

        for row in reader:
            tag = row[3]
            if is_verb.match(tag):
                nb_verbs += 1
                if is_annotated_verb(row):
                    nb_tagged += 1
                    if tag == cattex09_verb_inf:
                        nb_tagged_inf += 1
                    elif tag == cattex09_verb_ppa:
                        nb_tagged_ppa += 1
                    elif tag == cattex09_verb_ppe:
                        nb_tagged_ppe += 1
                    elif tag == cattex09_verb_cjg:
                        nb_tagged_cjg += 1

    print(nb_verbs)
    print(nb_tagged)
    print(f"VERinf : {nb_tagged_inf}")
    print(f"VERppa : {nb_tagged_ppa}")
    print(f"VERppe : {nb_tagged_ppe}")
    print(f"VERcjg : {nb_tagged_cjg}")
    print(f"Pourcentage de verbes annotés : {nb_tagged / nb_verbs * 100} %")
    print(f"Pourcentage de verbes conjugués annotés : {nb_tagged_cjg / 16815 * 100} %")


if __name__ == '__main__':
    get_tagged_stats()
