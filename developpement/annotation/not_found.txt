118489 mots en tout.
22502 verbes en tout.
Nombre de verbes annotés à la première étape : 17543 ; à la deuxième étape : 5687 ; aux deux étapes : 3174
Nombre de verbes conjugués annotés à la première étape : 14369 ; à la deuxième étape : 0 ; aux deux étapes : 0
Nombre de verbes à l'infinitif annotés à la première étape : 1759 ; à la deuxième étape : 2313 ; aux deux étapes : 1759
Nombre de verbes au participe passé annotés à la première étape : 1297 ; à la deuxième étape : 3148 ; aux deux étapes : 1297
Nombre de verbes au participe présent annotés à la première étape : 118 ; à la deuxième étape : 226 ; aux deux étapes : 118