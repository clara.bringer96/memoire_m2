import csv
import re
import xml.etree.ElementTree as ET
from _csv import reader, writer
from pathlib import Path
from typing import Dict, List
from xml.etree.ElementTree import Element

current = Path.cwd()
morphalou_like_file = current / ".." / "bdd_tcaf" / "morphalou_like_bdd.xml"
verbs_tree = ET.parse(str(morphalou_like_file))

is_verb = re.compile(r"^VER")
get_inf = re.compile(r"(.+)\d+$")

annotated_corpus = current / ".." / "corpus" / "LGeRM" / "utf8_qgraal_LGeRM.csv"
output = "tagged_utf8_qgraal_LGeRM.csv"
stats_file = "not_found.txt"

cattex09max_moods = {
    "indicative": "ind",
    "subjunctive": "sub",
    "conditional": "con",
    "imperative": "imp"
}

cattex09max_tenses = {
    "present": "pst",
    "past": "pas",
    "simplePast": "psp",
    "imperfect": "ipf",
    "archaicPast": "arp",
    "future": "fut"
}

cattex09max_persons = {
    "firstPerson": "1",
    "secondPerson": "2",
    "thirdPerson": "3"
}

cattex09max_numbers = {"singular": "s", "plural": "p"}

cattex09_verb_cjg = "VERcjg"
cattex09_verb_inf = "VERinf"
cattex09_verb_ppe = "VERppe"
cattex09_verb_ppa = "VERppa"

lemmatized_form = "lemmatizedForm"
orthography = "orthography"
lemmatized_orthography = "lemmatizedOrthography"
grammatical_mood = "grammaticalMood"
grammatical_tense = "grammaticalTense"
grammatical_person = "grammaticalPerson"
grammatical_number = "grammaticalNumber"


def cattex09max_inflection(inflection: Dict[str, str]) -> str:
    """
    Convert the inflection into Cattex09max format
    :param inflection: inflection to convert
    :return: string containing the Cattex09max formatted inflection
    """
    cattex_inflection = ""
    lemma = inflection[lemmatized_orthography] if lemmatized_orthography in inflection else ""
    mood = inflection[grammatical_mood] if grammatical_mood in inflection else ""
    if mood == "infinitive":
        cattex_inflection = f"{cattex09_verb_inf}/{lemma}"
    elif mood == "participle":
        tense = inflection[grammatical_tense] if "" in inflection else ""
        if tense == "past":
            cattex_inflection = f"{cattex09_verb_ppe}/{lemma}"
        elif tense == "present":
            cattex_inflection = f"{cattex09_verb_ppa}/{lemma}"
    else:
        cattex_inflection = cattex09_verb_cjg

        tense = inflection[grammatical_tense] if grammatical_tense in inflection else ""
        person = inflection[grammatical_person] if grammatical_person in inflection else ""
        number = inflection[grammatical_number] if grammatical_number in inflection else ""

        cattex_mood = cattex09max_moods[mood] if mood in cattex09max_moods else None
        cattex_tense = cattex09max_tenses[tense] if tense in cattex09max_tenses else None
        cattex_person = cattex09max_persons[person] if person in cattex09max_persons else None
        cattex_number = cattex09max_numbers[number] if number in cattex09max_numbers else None

        if cattex_mood is not None:
            cattex_inflection += f"/{cattex09max_moods[mood]}"
        if cattex_tense is not None:
            cattex_inflection += f"/{cattex09max_tenses[tense]}"
        if cattex_person is not None:
            cattex_inflection += f"/{cattex09max_persons[person]}"
        if cattex_number is not None:
            cattex_inflection += f"/{cattex09max_numbers[number]}"
        cattex_inflection += f"/{lemma}"

    return cattex_inflection


def get_inflection(form_set: Element, inflected_form: Element) -> Dict[str, str]:
    """
    Get the information for one inflection of a verb
    :param form_set: formSet tag found in the XML database
    :param inflected_form: inflectedForm tag found in the XML database
    :return: dictionary containing the mood, the tense, the person and the lemma of the inflection
    """
    lemmatized_form_tag = form_set.find(lemmatized_form)

    inflection = dict()

    mood_found = inflected_form.find(grammatical_mood)
    if mood_found is not None:
        inflection[grammatical_mood] = mood_found.text.strip()
    tense_found = inflected_form.find(grammatical_tense)
    if tense_found is not None:
        inflection[grammatical_tense] = tense_found.text.strip()
    person_found = inflected_form.find(grammatical_person)
    if person_found is not None:
        inflection[grammatical_person] = person_found.text.strip()
    number_found = inflected_form.find(grammatical_number)
    if number_found is not None:
        inflection[grammatical_number] = number_found.text.strip()

    if len(inflection) > 0:
        inflection[lemmatized_orthography] = lemmatized_form_tag.find(orthography).text.strip()

    return inflection


def get_inflections(to_find: str) -> List[Dict[str, str]]:
    """
    Get the information for all inflections of a verb
    :param to_find: verb to find in the XML database
    :return: list of all inflections
    """
    found = [(form_set, inflected_form) for form_set in verbs_tree.findall("lexicalEntry/formSet")
             for inflected_form in form_set.findall("*")
             if inflected_form.find(orthography) is not None and
             inflected_form.find(orthography).text.strip().lower() == to_find.lower()
             ]

    inflections = []
    for form_set, inflected_form in found:
        inflection = get_inflection(form_set, inflected_form)
        if inflection is not None and inflection != dict():
            inflections.append(inflection)

    return inflections


def get_set_inflections(inflections: List[Dict[str, str]]) -> List[Dict[str, str]]:
    """
    Get a list of unique inflections
    :param inflections: list of inflections
    :return: same list without duplication
    """
    set_inflections: List[Dict[str, str]] = []
    for inflection in inflections:
        if inflection not in set_inflections:
            set_inflections.append(inflection)

    return set_inflections


def order_row(row: List[str]) -> List[str]:
    """
    Get the row ordered by the tag of the verb
    :param row: row of the file to order
    :return: same row with order
    """
    ordered_row = row[:4]
    tag = ordered_row[3]
    inflection = row[4]

    like_tag = [r for r in inflection.split("|") if tag in r]
    tmp_row = [r for r in inflection.split("|") if r not in like_tag]
    tags = like_tag + tmp_row

    ordered_row.append("|".join(tags))

    return ordered_row


def extraction(csv_reader: reader, csv_writer: writer, stat_file: str, improve: bool = False):
    """
    Extract all the inflections for all verbs in the text, and store statistics
    :param csv_reader: object to read the CSV file
    :param csv_writer: object to write in the CSV file
    :param stat_file: file to store statistics in
    :param improve: flag to include improvments or not
    :return: None
    """
    # Number of verbs in the text
    nb_verbs = 0

    # Number of verbs that are annotated, with 1st method, with 2nd method, with both methods
    nb_verbs_annotated = [0, 0, 0]
    # Number of conjugated verbs that are annotated, with 1st method, with 2nd method, with both methods
    nb_verbs_cjg_annotated = [0, 0, 0]
    # Number of infinitive verbs that are annotated, with 1st method, with 2nd method, with both methods
    nb_verbs_inf_annotated = [0, 0, 0]
    # Number of past participle verbs that are annotated, with 1st method, with 2nd method, with both methods
    nb_verbs_ppe_annotated = [0, 0, 0]
    # Number of present participle verbs that are annotated, with 1st method, with 2nd method, with both methods
    nb_verbs_ppa_annotated = [0, 0, 0]

    i = 0

    for i, row in enumerate(csv_reader):
        # Printer to check the running of the script
        if i % 100 == 0:
            print(i)
        tag = row[3]

        if is_verb.match(tag):
            nb_verbs += 1
            found = False

            # Verb to find in the XML database
            to_find = row[0]
            # All the inflections found for this verb
            inflections = get_inflections(to_find)

            if len(inflections) > 0:
                found = True
                nb_verbs_annotated[0] += 1

                if tag == cattex09_verb_cjg:
                    nb_verbs_cjg_annotated[0] += 1
                elif tag == cattex09_verb_inf:
                    nb_verbs_inf_annotated[0] += 1
                elif tag == cattex09_verb_ppe:
                    nb_verbs_ppe_annotated[0] += 1
                elif tag == cattex09_verb_ppa:
                    nb_verbs_ppa_annotated[0] += 1

            # Improvments
            if improve:
                infs = row[1].split("|")
                infinitives = []
                for inf in infs:
                    m = get_inf.match(inf)
                    if m:
                        infinitives.append(m.group(1).lower())
                    else:
                        infinitives.append(inf.lower())

                if tag == cattex09_verb_inf:
                    inflections.append({grammatical_mood: "infinitive", lemmatized_orthography: to_find})
                    for infinitive in infinitives:
                        if infinitive != to_find:
                            inflections.append({grammatical_mood: "infinitive",
                                                lemmatized_orthography: infinitive})

                    nb_verbs_annotated[1] += 1
                    nb_verbs_inf_annotated[1] += 1

                    if found:
                        nb_verbs_annotated[2] += 1
                        nb_verbs_inf_annotated[2] += 1

                if tag == cattex09_verb_ppa:
                    improved = False
                    for infinitive in infinitives:
                        inflections.append({grammatical_mood: "participle", grammatical_tense: "present",
                                            lemmatized_orthography: infinitive})
                        improved = True

                    if improved:
                        nb_verbs_annotated[1] += 1
                        nb_verbs_ppa_annotated[1] += 1

                    if found and improved:
                        nb_verbs_annotated[2] += 1
                        nb_verbs_ppa_annotated[2] += 1

                if tag == cattex09_verb_ppe:
                    improved = False
                    for infinitive in infinitives:
                        inflections.append({grammatical_mood: "participle", grammatical_tense: "past",
                                            lemmatized_orthography: infinitive})
                        improved = True

                    if improved:
                        nb_verbs_annotated[1] += 1
                        nb_verbs_ppe_annotated[1] += 1

                    if found and improved:
                        nb_verbs_annotated[2] += 1
                        nb_verbs_ppe_annotated[2] += 1

            row.append("|".join([cattex09max_inflection(inflection)
                                 for inflection in get_set_inflections(inflections)]))

            # Ordering the inflections
            row = order_row(row)

        csv_writer.writerow(row)

    # Saving the statistics of this extraction
    with open(stat_file, "w", encoding="utf8") as saving:
        saving.write(f"{i} mots en tout.\n")
        saving.write(f"{nb_verbs} verbes en tout.\n")
        saving.write(f"Nombre de verbes annotés à la première étape : {nb_verbs_annotated[0]} ; "
                     f"à la deuxième étape : {nb_verbs_annotated[1]} ; "
                     f"aux deux étapes : {nb_verbs_annotated[2]}\n")
        saving.write(f"Nombre de verbes conjugués annotés à la première étape : {nb_verbs_cjg_annotated[0]} ; "
                     f"à la deuxième étape : {nb_verbs_cjg_annotated[1]} ; "
                     f"aux deux étapes : {nb_verbs_cjg_annotated[2]}\n")
        saving.write(f"Nombre de verbes à l'infinitif annotés à la première étape : {nb_verbs_inf_annotated[0]} ; "
                     f"à la deuxième étape : {nb_verbs_inf_annotated[1]} ; "
                     f"aux deux étapes : {nb_verbs_inf_annotated[2]}\n")
        saving.write(f"Nombre de verbes au participe passé annotés à la première étape : {nb_verbs_ppe_annotated[0]} ; "
                     f"à la deuxième étape : {nb_verbs_ppe_annotated[1]} ; "
                     f"aux deux étapes : {nb_verbs_ppe_annotated[2]}\n")
        saving.write(f"Nombre de verbes au participe présent annotés à la première étape : {nb_verbs_ppa_annotated[0]} "
                     f"; à la deuxième étape : {nb_verbs_ppa_annotated[1]} ; "
                     f"aux deux étapes : {nb_verbs_ppa_annotated[2]}")


if __name__ == '__main__':
    with open(str(annotated_corpus), newline="") as csv_verbs:
        csv_reader = csv.reader(csv_verbs, delimiter=";", quotechar=None)
        with open(output, "w", newline="") as csv_out:
            csv_writer = csv.writer(csv_out, delimiter=";", quotechar=None)

            extraction(csv_reader, csv_writer, stats_file, True)
