import csv
import re
from pathlib import Path

from bs4 import BeautifulSoup

tag_re = re.compile(r"<\w*>(\w+)")
compiler = re.compile(r"([a-zA-Zéçï]*)[\-0-9]+([a-zA-Zéçï]*)")
end_cut = re.compile(r".+-$")
brackets = re.compile(r"(.*)\[(.*)\](.*)")


def clean_span(text):
    b = brackets.search(text)
    text = f"{b.group(1)}{b.group(2)}{b.group(3)}" if b else text

    c = compiler.search(text)
    text = f"{c.group(1)}{c.group(2)}" if c else text

    return text


def read_html(file, writer, start):
    with open(file, "r", encoding="utf8") as html:
        soup = BeautifulSoup(html, "html.parser")
        span_class = soup.find_all("span", attrs={"class": "word"})

        for span in span_class:
            s = tag_re.search(span.text)
            text = s.group(1) if s else span.text

            if len(start) > 0:
                text = start + text
                start = ""
            if end_cut.match(text):
                start = text[:-1]
            else:
                row = [clean_span(text), span["title"]]
                writer.writerow(row)

    return start


def read_rep(rep, output_file):
    with open(str(output_file), "w", newline="") as output:
        writer = csv.writer(output, quotechar="\"", delimiter=";")
        files_path = Path(rep)
        if files_path.exists() and files_path.is_dir():
            start = ""
            for f in sorted(files_path.iterdir()):
                print(f)
                start = read_html(f, writer, start)


if __name__ == '__main__':
    current = Path.cwd()
    rep = current / "TXM" / "graal_TXM" / "HTML" / "GRAAL" / "courante"
    output_file = current / "TXM" / "tmp_annotated_corpus.csv"

    read_rep(rep, output_file)
