import csv
import re
from pathlib import Path

current = Path.cwd()
file_txm_csv = current / "TXM" / "annotated_corpus.csv"
file_lgerm_csv = current / "LGeRM" / "utf8_qgraal_LGeRM.csv"

is_verb = re.compile(r"^VER")


def get_numbers_text(file, index_tag):
    nb_words = 0
    verbs = dict()
    nb_verbs = 0
    other_tags = set()

    with open(str(file), newline="") as csv_file:
        reader = csv.reader(csv_file, delimiter=";", quotechar=None)

        for line in reader:
            if len(line[0]) > 0:
                nb_words += 1
                tag = line[index_tag]
                if is_verb.match(tag) is not None:
                    nb_verbs += 1
                    if tag not in verbs:
                        verbs[tag] = [0, set()]
                    verbs[tag][0] += 1
                    verbs[tag][1].add(line[0].lower())
                else:
                    other_tags.add(line[index_tag])

    return nb_words, verbs


def get_numbers_txm():
    return get_numbers_text(file_txm_csv, 1)


def get_numbers_lgerm():
    return get_numbers_text(file_lgerm_csv, 3)


def get_number_verbs(verbs):
    return sum([v[0] for t, v in verbs.items()])


def get_different_verbs(verbs):
    return {f for t, v in verbs.items() for f in v[1]}


def get_number_verbs_tag(verbs):
    return {tag: [v[0], len(v[1])] for tag, v in verbs.items()}


def get_differences(verbs_lgerm, verbs_txm):
    set_verbs_lgerm = get_different_verbs(verbs_lgerm)
    set_verbs_txm = get_different_verbs(verbs_txm)

    diff_lgerm_txm = set_verbs_lgerm - set_verbs_txm
    diff_txm_lgerm = set_verbs_txm - set_verbs_lgerm

    return diff_lgerm_txm, diff_txm_lgerm


if __name__ == '__main__':
    nb_words_lgerm, verbs_lgerm = get_numbers_lgerm()

    print("LGeRM", nb_words_lgerm, verbs_lgerm["VERcjg"][0], verbs_lgerm["VERinf"][0], verbs_lgerm["VERppe"][0],
          verbs_lgerm["VERppa"][0])

    nb_words_txm, verbs_txm = get_numbers_txm()

    print("TXM", nb_words_txm, verbs_txm["VERcjg"][0], verbs_txm["VERinf"][0], verbs_txm["VERppe"][0],
          verbs_lgerm["VERppa"][0])

    print("LGeRM", get_number_verbs_tag(verbs_lgerm))
    print("TXM", get_number_verbs_tag(verbs_txm))

    diff_lgerm_txm, diff_txm_lgerm = get_differences(verbs_lgerm, verbs_txm)

    print(f"LGeRM : {len(diff_lgerm_txm)}", f"TXM : {len(diff_txm_lgerm)}")
    print(f"LGeRM : {diff_lgerm_txm}", f"TXM : {diff_txm_lgerm}")
